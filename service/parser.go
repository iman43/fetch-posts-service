package service

import (
	"encoding/json"
	"net/http"
)

func bodyParser(r *http.Response, body interface{}) error {
	return json.NewDecoder(r.Body).Decode(&body)
}
