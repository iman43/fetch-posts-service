package service

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"os"
	"sync"
	"time"

	fetchPb "github.com/doston/iman/fetch_post/genproto/fetch_post_service"
	postPb "github.com/doston/iman/fetch_post/genproto/post_service"
	postClient "github.com/doston/iman/fetch_post/grpc_client"

	"context"
	"fmt"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "fuser"
	password = "9585"
	dbname   = "iman"
)

type server struct {
	fetchPb.UnimplementedFetchPostServiceServer
	storage sql.DB
}

func createConnection() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)

	if err != nil {
		panic(err)
	}

	err = db.Ping()

	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully connected")

	return db
}

func New() *server {

	db := createConnection()

	return &server{storage: *db}

}

type FetchPostsResponse struct {
	Meta  Meta   `json:"meta"`
	Posts []Post `json:"data"`
	mu    sync.Mutex
}

type Meta struct {
	Pagination Pagination `json:"pagination"`
}

type Pagination struct {
	Total int32 `json:"total"`
	Pages int32 `json:"pages"`
	Page  int32 `json:"page"`
	Limit int32 `json:"limit"`
	Link  Link  `json:"links"`
}

type Link struct {
	Previous string `json:"previous"`
	Current  string `json:"current"`
	Next     string `json:"next"`
}

type Post struct {
	ID     int64  `json:"id"`
	UserID int64  `json:"user_id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}

type safePosts struct {
	posts []*postPb.Post
	mu    sync.Mutex
}

func (s *server) FetchPosts(ctx context.Context, in *fetchPb.FetchPostsRequest) (*fetchPb.Empty, error) {
	client := &http.Client{Timeout: 10 * time.Second}
	var i int32
	var url string
	posts := make([]*postPb.Post, 0, 60)
	safePosts := safePosts{
		posts: posts,
		mu:    sync.Mutex{},
	}

	// mu := sync.Mutex{}
	// wg := sync.WaitGroup{}
	// for i = 0; i <= in.NumberOfPages; i++ {
	// 	// wg.Add(1)
	// 	var url string
	// 	go func(j int32) {
	// 		baseUrl := "https://gorest.co.in/public/v1/posts"
	// 		fmt.Println("in routine", j)
	// 		if j == 0 {
	// 			url = baseUrl
	// 		} else {
	// 			url = fmt.Sprintf("%s?page=%d", baseUrl, j)
	// 		}
	// 		resp, err := client.Get(url)
	// 		if err != nil {
	// 			fmt.Printf(err.Error())
	// 			// return nil, err
	// 			os.Exit(1)
	// 		}
	// 		defer resp.Body.Close()
	// 		var (
	// 			body FetchPostsResponse
	// 		)
	// 		json.NewDecoder(resp.Body).Decode(&body)
	// 		if err != nil {
	// 			fmt.Printf(err.Error())
	// 			// return nil, err
	// 			os.Exit(1)
	// 		}
	// 		fmt.Println("body data:", body.Meta.Pagination.Page)
	// 		// wg.Done()
	// 	}(i)
	// }
	// wg.Wait()
	// req := `INSERT INTO posts (book_name, price) VALUES ($1, $2) RETURNING id`
	// var id int32
	// err = s.storage.QueryRow(req, in.NumberOfPages).Scan(&id)
	// if err != nil {
	// 	fmt.Printf("error %v", err)
	// 	log.Println(err)
	// 	return nil, err
	// }
	wg := sync.WaitGroup{}

	go func(nPage int32) {

		var j int32
		for j = nPage; j <= 2*nPage; j++ {

			clientIn := &http.Client{Timeout: 10 * time.Second}

			baseUrlIn := "https://gorest.co.in/public/v1/posts?page="
			urlIn := fmt.Sprintf("%s%d", baseUrlIn, j)

			resp, err := clientIn.Get(urlIn)
			if err != nil {
				fmt.Printf(err.Error())
				os.Exit(1)
			}
			defer resp.Body.Close()
			var (
				body FetchPostsResponse
			)
			json.NewDecoder(resp.Body).Decode(&body)
			if err != nil {
				fmt.Printf(err.Error())
				os.Exit(1)
			}

			p := []*postPb.Post{}
			for i := 0; i < 10; i++ {
				tempP := postPb.Post{
					Id:     body.Posts[i].ID,
					UserId: body.Posts[i].UserID,
					Title:  body.Posts[i].Title,
					Body:   body.Posts[i].Body,
				}
				p = append(p, &tempP)
			}

			safePosts.mu.Lock()
			safePosts.posts = append(safePosts.posts, p...)
			safePosts.mu.Unlock()
			// wg.Done()
		}
	}(in.NumberOfPages / 2)

	baseUrl := "https://gorest.co.in/public/v1/posts?page="

	for i = 1; i < in.NumberOfPages/2; i++ {

		url = fmt.Sprintf("%s%d", baseUrl, i)

		resp, err := client.Get(url)
		if err != nil {
			fmt.Printf(err.Error())
			os.Exit(1)
		}
		defer resp.Body.Close()
		var (
			body FetchPostsResponse
		)
		json.NewDecoder(resp.Body).Decode(&body)
		if err != nil {
			fmt.Printf(err.Error())
			os.Exit(1)
		}

		p := []*postPb.Post{}
		for i := 0; i < 10; i++ {
			tempP := postPb.Post{
				Id:     body.Posts[i].ID,
				UserId: body.Posts[i].UserID,
				Title:  body.Posts[i].Title,
				Body:   body.Posts[i].Body,
			}
			p = append(p, &tempP)
		}

		safePosts.mu.Lock()
		safePosts.posts = append(safePosts.posts, p...)
		safePosts.mu.Unlock()

	}

	wg.Wait()

	_, err := postClient.PostService().CreatePost(
		context.Background(), &postPb.CreatePostRequest{Posts: safePosts.posts},
	)

	if err != nil {
		fmt.Println("Error while inserting posts", err.Error())
		return nil, err
	}

	return &fetchPb.Empty{}, nil
}
