package grpcclient

import (
	"fmt"
	"sync"

	"github.com/doston/iman/fetch_post/configs"
	post "github.com/doston/iman/fetch_post/genproto/post_service"
	"google.golang.org/grpc"
)

var cfg = configs.Config()
var (
	onceUPostService   sync.Once

	instancePostService     post.PostServiceClient

)

// PostService ...
func PostService() post.PostServiceClient {
	onceUPostService.Do(func() {
		connPost, err := grpc.Dial(
			fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
			grpc.WithInsecure())
		if err != nil {
			panic(fmt.Errorf("post service dial host: %s port:%d err: %s",
				cfg.PostServiceHost, cfg.PostServicePort, err))
		}

		instancePostService = post.NewPostServiceClient(connPost)
	})

	return instancePostService
}