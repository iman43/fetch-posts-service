package configs

import (
	"errors"
	"sync"

	"github.com/spf13/viper"
)

var (
	instance *Configuration
	once     sync.Once
)

//Config ...
func Config() *Configuration {
	once.Do(func() {
		instance = load()
	})

	return instance
}

// Configuration ...
type Configuration struct {
	HTTPPort string `json:"http_port"`

	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string

	PostServiceHost string
	PostServicePort int

}

func load() *Configuration {

	var config Configuration

	v := viper.New()
	v.AutomaticEnv()
	v.SetDefault("POSTGRES_HOST", "localhost")
	v.SetDefault("POSTGRES_PORT", 5432)
	v.SetDefault("POSTGRES_USER", "fuser")
	v.SetDefault("POSTGRES_PASSWORD", "9585")
	v.SetDefault("POSTGRES_DB", "iman")
	v.SetDefault("CONTEXT_TIMEOUT", 7)
	v.SetDefault("POST_SERVICE_HOST", "localhost")
	v.SetDefault("POST_SERVICE_PORT", 9001)

	config.PostgresDatabase = v.GetString("POSTGRES_DB")
	config.PostgresUser = v.GetString("POSTGRES_USER")
	config.PostgresPassword = v.GetString("POSTGRES_PASSWORD")
	config.PostgresHost = v.GetString("POSTGRES_HOST")
	config.PostgresPort = v.GetInt("POSTGRES_PORT")
	config.PostServiceHost = v.GetString("POST_SERVICE_HOST")
	config.PostServicePort = v.GetInt("POST_SERVICE_PORT")
	return &config
}

//Validate validates the configuration
func (c *Configuration) Validate() error {
	if c.HTTPPort == "" {
		return errors.New("http_port required")
	}
	return nil
}
