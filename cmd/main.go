package main

import (
	"log"
	"net"

	pb "github.com/doston/iman/fetch_post/genproto/fetch_post_service"
	fetch "github.com/doston/iman/fetch_post/service"

	_ "github.com/lib/pq" // postgres golang driver
	"google.golang.org/grpc"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "fuser"
	password = "9585"
	dbname   = "post"
)

const (
	serverPort = ":9000"
)

func main() {
	lis, err := net.Listen("tcp", serverPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	fetchService := fetch.New()

	s := grpc.NewServer()
	pb.RegisterFetchPostServiceServer(s, fetchService)
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
